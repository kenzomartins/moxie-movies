## Read Me
This Wordpress plugin installs a movie catalogue on your wordpress website

#Installation
- copy and paste plugin into wordpress home directory
- ensure you have proper permission especially if you are running on *unix environment
- install plugin; if installation fails on linux due to permission 
  run: sudo chmod 777 -R /var/www/wordpress/wp-content/ to grant all permission to script
  within that directory
  or copy and paste the mxmovie folder in the theme directory of the plugin to wordpress 
  theme directory and try again
- visit homepage to view plugin in action