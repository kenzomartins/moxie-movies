<?php
/**
 * Plugin Name: MOXIE MOVIES
 * Description: Adds a movie catalogue to your website. (Ensure you are running PHP >= 5.4)
 * Version: 0.1
 * Author: Tobi Martins
 * Author URI: https://www.linkedin.com/profile/view?id=186404149
 */

define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);

//register all hooks that needs registering 
register_activation_hook(__FILE__, 'MXinstall');
register_uninstall_hook(__FILE__, 'MXuninstall');
register_deactivation_hook(__FILE__, 'MXdeactivation');
add_action('init', 'MXmovie_register');
add_action('save_post', 'MXsave_meta');
add_action("admin_init", "MXmovie_manager");
add_action('parse_request', 'MXAPI_handler');
add_action("manage_posts_custom_column", "MXfill_cp_column");
add_filter("manage_edit-mxmovie_columns", "MXadd_cp_column");

/**
 * wrap plugin installation
 * Note: this plugin doesn't require any special wordpress permission
 */
function MXinstall() {

 if ((get_option('isInstalled')) === false) {
    //This is a fresh installation so load every neccessay data
    if (!@MXinitPersistence()) {
      //display apprioprate error message
      exit("Oops! something went wrong while uploading necessary data needed for proper functioning, please contact your site administrator");
    }

    //install theme into wordpress theme directory
    if (!file_exists(__DIR__ . '/../../themes/mxmovie') || count(scandir(__DIR__ . '/../../themes/mxmovie')) <= 2) {
      if (!__MXrecursive_copy(__DIR__ . '/theme/mxmovie', __DIR__ . '/../../themes/mxmovie')
              || count(scandir(__DIR__ . '/../../themes/mxmovie')) <= 2) {
        //display apprioprate error message
        exit('Could not copy files into theme directory, ensure you have necessary permissions or manually copy the <b>mxmovie</b> folder in the theme directory of the plugin to wordpress theme directory and try again.');
      }
    }
    
    //switch theme to use custom theme bundled with this plugin
    switch_theme('mxmovie', 'mxmovie');
    //flag this installation as succesful
    add_option('MXIsInstalled', 1);
  }
}

/**
 * wrap plugin uninstallation
 * @global type $wp_post_types
 */
function MXuninstall() {

  global $wp_post_types;

  //remove data created by plugin
  if (!MXDestroyPersistence()) {
    //since this failed, display apprioprate error message
    exit("Oops! something went wrong, could not uninstall plugin; please contact your site administrator");
  }

  //remove CPT from menu
  if (isset($wp_post_types['MXmovie'])) {
    unset($wp_post_types['MXmovie']);
    remove_menu_page('edit.php?post_type=MXmovie');
  }
  
  //removes directory (if this fails due to permission
  //it shouldn't stop us from uninstalling the plugin).
  __MXrecursive_delete(__DIR__ . '/../../themes/mxmovie');
  
  //revert back to theme 
  switch_theme('twentyfifteen', 'twentyfifteen');
  
  //delete installation flag
  delete_option('MXIsInstalled');
}

/**
 * Cleanup system for plugin deactivation
 */
function MXdeactivation() {
  switch_theme('twentyfifteen', 'twentyfifteen');
}

/**
 * Set custom CPT to show below in admin area
 */
function MXmovie_manager() {
  add_meta_box("movie_meta_info", "Additional Information", "__MXmovie_meta_info", "MXmovie", "normal", "low");
}

/**
 * HTML display for saving/editing CPT 
 * @global type $post
 */
function __MXmovie_meta_info() {
  global $post;

  $custom = get_post_custom($post->ID);
  $rating = $custom["rating"][0];
  $url = $custom["url"][0];
  $year = $custom["year"][0];
  ?>
  <p>
    <label>Rating:</label><br />
    <input type="number" min="0.0" step="0.1" max="10.0" name="rating" value="<?php echo $rating; ?>"/> 
  </p>
  <p>
    <label>URL:</label><br />
    <input type="text" name="url" value="<?php echo $url; ?>"/> 
  </p>
  <p>
    <label>Year:</label><br />
    <input type="number" min="1970" max="<?php echo gmdate('YY'); ?>" name="year" value="<?php echo $year; ?>"/>
  </p>
  <?php
}

/**
 * Persists CBT data
 * @global type $post
 */
function MXsave_meta() {
  global $post;

  //performs an insert for new data and an update for existing ones
  update_post_meta($post->ID, "rating", $_POST["rating"]);
  update_post_meta($post->ID, "url", $_POST["url"]);
  update_post_meta($post->ID, "year", $_POST["year"]);
}

/**
 * Include custom fields in admin area, (This is what you see 
 * in the admin area when movies are listed)
 * @return string
 */
function MXadd_cp_column() {
  $columns = [
      "cb" => "<input type=\"checkbox\" />",
      "title" => "Movie Title",
      "description" => "Description",
      "rating" => "Rating",
      "year" => "Year"
  ];

  return $columns;
}

/**
 * Populate custom fields with data, (This fills data into the columns 
 * you see in the admin area when movies are listed)
 * @global type $post
 */
function MXfill_cp_column($column) {
  global $post;

  //extract information needed by each field
  switch ($column) {
    case "description":
      the_excerpt();
      break;
    case "year":
      $custom = get_post_custom();
      echo $custom["year"][0];
      break;
    case "rating":
      $custom = get_post_custom();
      echo $custom["rating"][0];
      break;
  }
}

/**
 * load data into persistent storage 
 * @global type $wpdb
 * @global type $config
 * @return boolean
 */
function MXinitPersistence() {

  global $wpdb;
  global $config;

  //prepare random data to be imported into DB   
  //read data from csv and insert as wordpress posts
  if (!$file_rs = fopen(__DIR__ . '/data.csv', 'r'))
    return false;

  $count = 0;
  $data = [];
  while (($row = fgetcsv($file_rs, 0, ',', '"')) !== FALSE) {
    if (++$count == 1 || count($row) != 5)
      continue;

    //populate CPT
    $post_id = wp_insert_post([
        'post_type' => 'MXmovie',
        'post_title' => $row[0],
        'post_content' => $row[3],
        'post_status' => 'publish'
    ]);

    if ($post_id) {
      // insert post meta
      add_post_meta($post_id, 'rating', $row[1]);
      add_post_meta($post_id, 'url', $row[2]);
      add_post_meta($post_id, 'year', $row[4]);
    } else
      return false;
  } fclose($file_rs);
  
  return true;
}

/**
 * Delete every data created by this plugin
 * @return boolean
 */
function MXDestroyPersistence() {

  //query posts by post type
  $filter = [
      'post_type' => 'MXmovie',
      'posts_per_page' => -1
  ];
  $posts = new WP_Query($filter);

  $posts = $posts->posts;
  foreach ($posts as $post) {
    // it's always adviseable not to delete information,
    // soo... let's trash instead of deleting
    if(!wp_delete_post($post->ID, false)) return false;
  }

  return true;
}


/**
 * Copies directory from one folder to another
 */
function __MXrecursive_copy($src, $dst) {

  //open source directory and return handle
  $dir = @opendir($src);
  //create destination directory with appropriate permission
  if (!@mkdir($dst, 0777, true))
    return false;

  //read from source and copy recursively to destination
  while (false !== ( $file = readdir($dir))) {
    if (( $file != '.' ) && ( $file != '..' )) {
      if (is_dir($src . DIRECTORY_SEPARATOR . $file)) {
        __MXrecursive_copy($src . DIRECTORY_SEPARATOR . $file, $dst . DIRECTORY_SEPARATOR . $file);
      } else {
        if (!@copy($src . DIRECTORY_SEPARATOR . $file, $dst . DIRECTORY_SEPARATOR . $file))
          return false;
      }
    }
  }
  //remove handle to directory
  closedir($dir);
  return true;
}

/**
 * Deletes a directory recursively
 * @param type $dir
 */
function __MXrecursive_delete($dir) {
    //delete every file in specified directory
    foreach(scandir($dir) as $file) {
        if ('.' === $file || '..' === $file) continue;
        if (is_dir("$dir/$file")) __MXrecursive_delete("$dir/$file");
        else unlink("$dir/$file");
    }
    //delete empty folder
    rmdir($dir);
}

/**
 * Register new custom post type (CBT) for our plugin
 */
function MXmovie_register() {

  //configure labels used for our custom post type
  $labels = [
      'name' => _x('Movie', 'post type general name'),
      'singular_name' => _x('Movie Item', 'post type singular name'),
      'add_new' => _x('Add New', 'Movie item'),
      'add_new_item' => __('Add New Movie Item'),
      'edit_item' => __('Edit Movie Item'),
      'new_item' => __('New Movie Item'),
      'view_item' => __('View Movie Item'),
      'search_items' => __('Search Movie'),
      'not_found' => __('Nothing found'),
      'not_found_in_trash' => __('Nothing found in Trash')
  ];

  //CPT actual config
  $args = [
      'labels' => $labels,
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'menu_icon' => 'dashicons-format-video',
      'rewrite' => ['slug' => 'movies'],
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_position' => null,
      'supports' => ['title', 'editor', 'thumbnail']
  ];

  //create custom post type for movie persistence
  register_post_type('MXmovie', $args);
}


/**
 * Handle API requests to query for CPT data
 */
function MXAPI_handler() {
  
  //detect acceptable ajax submission
  if (isset($_GET['mxapi'])) {
    
    //filters query used for custom post
    $filter = [
        'post_type' => 'MXmovie',
        'cache_results' => true,
        'posts_fields' => 'id',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => -1
    ];    
    //query for post data
    $posts = new WP_Query($filter);

    //format results and output data
    $posts = $posts->posts;
    foreach ($posts as $post) {
      $data['data'][] = [
          'id' => $post->ID,
          'title' => $post->post_title,
          'poster_url' => get_post_meta($post->ID, 'url')[0],
          'rating' => get_post_meta($post->ID, 'rating')[0],
          'year' => get_post_meta($post->ID, 'year')[0],
          'short_description' => $post->post_content
      ];
    }

    echo json_encode($data);
    exit(0);
  }
}