<!DOCTYPE html>
<html lang="en">
    <head>
        <?php wp_head();?>
        <meta charset="utf-8">
        <title><?php wp_title(); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
        <![endif]-->

    </head>
    <body>
    <center>
        <div class="row header">
            <div class="col-md-12">
                <div id="header_movie_img">
                    <div class="header_movie_text">
                        <div class="header_movie_text_back">
                            <h1 id="mxtitle"></h1>
                            <label>Rating:</label> <span id="mxrating"></span>
                            <label>Year</label> <span id="mxyear"></span> <br/>
                            <span id="mxdesc"></span></div>   
                    </div>

                </div>
            </div>
        </div>
    </center>

    <div class="container voffset">
        <div class="row">

            <div class="col-md-2 column">
            </div>
            <!-- Column A -->
            <div id="colA" class="col-md-3 column ">

            </div>

            <!-- Column B -->
            <div id="colB" class="col-md-3 column ">

            </div>

            <!-- Column C -->
            <div id="colC" class="col-md-3 column ">

            </div>

        </div>
    </div>
    <center>
        <div class="row footer">
            <div class="col-md-12 text-muted">
                Copyright @ 2015, Moxie Group Inc.
            </div>
        </div>
    </center>
    <?php wp_footer();?>
</body>
</html>

