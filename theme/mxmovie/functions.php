<?php
/**
 * Enqueue neccesary scripts & styles for page.
 */

add_action( 'wp_enqueue_scripts', 'MXscript' );

function MXscript() {

	// Load main stylesheet.
	wp_enqueue_style( 'mx-style', get_stylesheet_uri() );
	wp_enqueue_style( 'mx-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
        wp_enqueue_style( 'mx-img', get_template_directory_uri() . 'img/favicon.png' );
       

	// Load custom app script
	wp_enqueue_script( 'mx-jquery',  get_template_directory_uri() . '/js/jquery.min.js', array( ), '', true );
	wp_enqueue_script( 'mx-bootstrap',  get_template_directory_uri() . '/js/bootstrap.min.js', array( ), '', true );
	wp_enqueue_script( 'mx-htmlshiv',  get_template_directory_uri() . '/js/html5shiv.js', array( ), '', true );	
	wp_enqueue_script( 'mx-js', get_template_directory_uri() . '/js/app.js', array(), '', true );

	// Variables for app script
	wp_localize_script( 'mx-js', 'mxJS',
		array(
			'api' => get_bloginfo( 'wpurl' ) . '/?mxapi',
		)
	);

}

